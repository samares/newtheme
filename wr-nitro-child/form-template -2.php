<?php
 /* Template Name: formtemplate2 */ 


// Get theme options
$wr_nitro_options = WR_Nitro::get_options();

// Get sidebar name
$wr_sidebar = isset( $wr_nitro_options['wr_page_layout_sidebar'] ) ? $wr_nitro_options['wr_page_layout_sidebar'] : '';

$wr_enable_page_builder = get_post_meta( get_the_ID(), '_wpb_vc_js_status', true );

get_header();
?>
	<?php
		if ( ! ( function_exists( 'is_account_page' ) && is_account_page() ) ) {
			WR_Nitro_Render::get_template( 'common/page', 'title' );
		}
	?>

	<?php if ( 'false' == $wr_enable_page_builder || empty( $wr_enable_page_builder ) || 'no-sidebar' != $wr_nitro_options['wr_page_layout'] ) echo '<div class="container mgt30 mgb30">'; ?>
		<div class="row page-content">
		
		<section class="form-section form_section">
<div class="container">
<div class="col-sm-12 col-md-12 col-lg-12">
<div class="form_style">
	   
	 <?php echo do_shortcode('[wpneo_crowdfunding_new_form]');?>
</div>			
</div>			
</div>			
</section>
		
		</div>
		
		
		

	<?php if ( 'false' == $wr_enable_page_builder || empty( $wr_enable_page_builder ) || 'no-sidebar' != $wr_nitro_options['wr_page_layout'] ) echo '</div>'; ?>

<?php get_footer(); ?>
