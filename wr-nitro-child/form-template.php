<?php
 /* Template Name: formtemplate */ 


// Get theme options
$wr_nitro_options = WR_Nitro::get_options();

// Get sidebar name
$wr_sidebar = isset( $wr_nitro_options['wr_page_layout_sidebar'] ) ? $wr_nitro_options['wr_page_layout_sidebar'] : '';

$wr_enable_page_builder = get_post_meta( get_the_ID(), '_wpb_vc_js_status', true );

get_header();
?>
	<?php
		if ( ! ( function_exists( 'is_account_page' ) && is_account_page() ) ) {
			WR_Nitro_Render::get_template( 'common/page', 'title' );
		}
	?>

	<?php if ( 'false' == $wr_enable_page_builder || empty( $wr_enable_page_builder ) || 'no-sidebar' != $wr_nitro_options['wr_page_layout'] ) echo '<div class="container mgt30 mgb30">'; ?>
		<div class="row page-content">
		<section class="form-section">
<div class="container">
<div class="col-sm-12 col-md-12 col-lg-12">
	   
	   <!-- progressbar -->
		<form id="msform">
			<!-- progressbar -->

		<ul id="progressbar">
				<li class="active">Step 1</li>
				<li>Step 2</li>
				<li>Step 3</li>
			</ul>
			<!-- fieldsets -->
			<fieldset>
			
				<h2 class="fs-title">Please, tell us for whom are you<br> planning ncahoots?</h2>
				
				
							<div class="form-group">
							<div class="col-sm-6 col-md-6">
                                <label for="username" class="text-info">I'm planning NCahoots for...</label>
								</div>
								<div class="col-sm-6 col-md-6">
								<span class="my-self">My Self</span>
                               <label class="switch">
							   
  <input type="checkbox"  onclick="history();" checked>
  <span class="slider round"></span>
 
</label>
 <span class="someone">Someone</span>
								</div>
                            </div>
                            <div class="form-group">
							<div class="col-sm-6 col-md-6">
                                <label for="password" class="text-info">Recepient's name is</label>
								</div>
								<div class="col-sm-6 col-md-6">
                                <input type="text" name="password" id="password" placeholder="Name" class="simple-input" style="border-bottom: 1px solid #ccc !important;">
								</div>
                            </div>
							  <div class="form-group">
							  <div class="col-sm-6 col-md-6">
                                <label for="username" class="text-info">Recepient's age is</label>
								</div>
								<div class="col-sm-6 col-md-6">
                                <input type="text" name="username" id="username" placeholder="21" class="simple-input" style="border-bottom: 1px solid #ccc !important;">
								</div>
                            </div>
                            <div class="form-group">
							<div class="col-sm-6 col-md-6">
                                <label for="password" class="text-info">Recepient's gender is</label>
								</div>
								<div class="col-sm-6 col-md-6">
                               <select style="border-bottom: 1px solid #ccc !important;">
							   <option>Male</option>
							   <option>Female</option>
							   </select>
								</div>
                            </div>
							<div class="form-group boxesrow_main">
										<div class="col-sm-6 col-md-6">
											<label for="password" class="text-info">How do you relate to the Recepient</label>
											</div>
											<div class="col-sm-6 col-md-6">
							<select style="border-bottom: 1px solid #ccc !important;">
										   <option>Friend </option>
										   <option>Boyfriend/Girlfriend</option>
											<option>Parent</option>
											 <option>Son/Daughter</option>
											  <option>Grandparent</option>
											   <option>Colleague</option>
												
											
									
										   </select>
									</div>
							</div>
				<div class="form-group">
                            <div class="col-sm-6 col-md-6">
                                <label for="password" class="text-info">Occation</label>
								</div>
								<div class="col-sm-6 col-md-6">
                                <select style="border-bottom: 1px solid #ccc !important;">
							   <option>Birthday </option>
							   <option>Wedding</option>
							    <option>Anniversary</option>
								 <option>Graduation</option>
								  <option>Christmas</option>
								   <option>Other</option>
								    
								
						
							   </select>
				</div>
				</div>
				
				
				<input type="button" name="next" class="next action-button" value="Next" />
			
				
				
			</fieldset>
			
			<fieldset>
			
				<h2 class="fs-title-secondsection">Set Your Goal</h2>
				<p class="fs-subtitle">Your can always upgrade your package if you will raise more money than expeected</p>
				<div class="inner-progress-sec">
				<ul>
				<li>$249</li>
				<li>$499</li>
				<li class="active">$699</li>
				<li>$999</li>
				<li>$1999</li>
			</ul>
				</div>
				
				<div class="inner-sec-form">
				<div class="top-label"><h4>Popular</h4></div>
				<h3>NCahoots Level 3</h3>
				
				<div class="col-sm-6 col-md-6">
				<div class="pro-imag">
				<img src="/wp-content/uploads/2019/05/ab.jpg">
				</div>
				</div>
				<div class="col-sm-6 col-md-6">
				<div class="right-site-data">
				<h4>Description</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
				<div class="unorder-list-data">
				<ul>
				<li>
				<i class="fa fa-star" aria-hidden="true"></i>
				<p>feature</p>
				</li>
				<li>
				<i class="fa fa-star" aria-hidden="true"></i>
				<p>feature</p>
				</li>
				<li>
				<i class="fa fa-star" aria-hidden="true"></i>
				<p>feature</p>
				</li>
				</ul>
				</div>
				</div>
				</div>
				
				</div>
							
				<input type="button" name="previous" class="previous action-button-second-back base" value="Back" />
				<input type="button" name="next" class="next action-button-second-next" value="Next" />
				
			</fieldset>
			<fieldset>
		
			
				<h2 class="fs-title">DELIVERY OPTIONS</h2>
				
				
				<div class="form-group">
							<div class="col-sm-5 col-md-5">
                                <label for="password" class="text-info">Delivery Date</label>
								<p class="fs-title form-group a"><b>IMPORTANT</b>: It takes us to create 7-14 days to create and deliver you NCc-hoots after you finaly raise the money.</p>
								</div>
								<div class="col-sm-2 col-md-2"></div>
								<div class="col-sm-5 col-md-4">
                                <input type="text" name="password" id="password" placeholder="12 April 19" class="simple-input" style="border-bottom: 1px solid #ccc !important;">
								</div>
                            </div>		
					
				
					
					<div class="form-group">
							<div class="col-sm-5 col-md-5">
                                <label for="username" class="text-info">Do you want us to deliver to you or to the Recipeints?</label>
								</div>
								<div class="col-sm-7 col-md-7">
								<div class="col-sm-12 col-md-12">
								<span class="my-self">To me</span>
                               <label class="switch">
							   
  <input type="checkbox"  onclick="history();" checked>
  <span class="slider round"></span>
 
</label>
 <span class="someone1">To the Recipient</span>
 </div>
								</div>
                            </div>
                            <div class="form-group">
							 <div class="col-sm-5 col-md-5">
                                <label for="password" class="text-info">Address</label>
								</div>
								<div class="col-sm-7 col-md-7">
								<div class="col-sm-12 col-md-12">
                                <input type="text" name="password" id="password" placeholder="Address Line1" class="simple-input" style="border-bottom: 1px solid #ccc !important;">
								</div>
								<div class="col-sm-12 col-md-12">
								<input type="text" name="password" id="password" placeholder="Address Line2" class="simple-input" style="border-bottom: 1px solid #ccc !important;">
								</div>
								<div class="col-sm-4 col-md-4">
								<input type="text" name="password" id="password" placeholder="City" class="simple-input" style="border-bottom: 1px solid #ccc !important;">
								</div>
								<div class="col-sm-4 col-md-4">
								 <input type="text" name="password" id="password" placeholder="State" class="simple-input" style="border-bottom: 1px solid #ccc !important;">
								 </div>
								 <div class="col-sm-4 col-md-4">
								 <input type="text" name="password" id="password" placeholder="Zip" class="simple-input" style="border-bottom: 1px solid #ccc !important;">
								 </div>
								</div>
                            </div>
							 
							 
							<div class="form-group">
							 <div class="col-sm-5 col-md-5">
                                <label for="password" class="text-info">Contact Phone Number</label>
								</div>
								<div class="col-sm-7 col-md-7">
								<div class="col-sm-12 col-md-12">
                                <input type="text" name="password" id="password" placeholder="Phone Number" class="simple-input" style="border-bottom: 1px solid #ccc !important;">
								</div>
								</div>
                            </div>
					
                      
                      
		
			
				<input type="button" name="previous" class="previous action-button-second-back" value="Back" />
				<input type="submit" onclick="location.href = 'http://sai.converzent.com/campaign/';"  name="submit" class="submit action-button-second-next-submit" value="Submit" />
			</fieldset>
		</form>
</div>			
</div>			
</section>
			<div class="fc fcw<?php echo ( $wr_nitro_options['wr_page_layout'] == 'right-sidebar' ) ? ' right-sidebar menu-on-right' : ''; ?>">
	
				<?php
					// Set page config
					$wr_args = array(
						'path'           => 'woorockets/templates',
						'layout'         => $wr_nitro_options['wr_page_layout'],
						'content_layout' => 'page',
						'sidebar'        => $wr_sidebar,
						'sidebar_class'  => 'primary-sidebar',
						'content_class'  => 'main-content',
					);

					WR_Nitro_Render::render_template( 'page', $wr_args );

					wp_link_pages( array(
						'before'      => '<div class="page-links"><span class="page-links-title">' . esc_html__( 'Pages:', 'wr-nitro' ) . '</span>',
						'after'       => '</div>',
						'link_before' => '<span>',
						'link_after'  => '</span>',
						'pagelink'    => '<span class="screen-reader-text">' . esc_html__( 'Page', 'wr-nitro' ) . ' </span>%',
						'separator'   => '<span class="screen-reader-text">, </span>',
					) );
				?>

			</div>
		</div>

	<?php if ( 'false' == $wr_enable_page_builder || empty( $wr_enable_page_builder ) || 'no-sidebar' != $wr_nitro_options['wr_page_layout'] ) echo '</div>'; ?>

<?php get_footer(); ?>
