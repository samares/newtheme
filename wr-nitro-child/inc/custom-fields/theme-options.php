<?php 

use Carbon_Fields\Container;
use Carbon_Fields\Field;

Container::make( 'theme_options', 'Настройки Темы' )
		  /*Section1 LOGO*/
	->add_tab( 'Лого', array( 
			Field::make( 'image', 'header_big_logo_img', 'Шапка сайта, логотип большой, 134x27px' )->set_value_type( 'url' )->set_width( 100 ),
	))
            
		    /*Section2 Contacts*/
	->add_tab( 'Контакты', array(
			Field::make( 'textarea', 'crb_full_address', 'Полный адрес' )->set_width( 100 )
			   ->help_text('Введите Полный адрес'),
		Field::make('text', 'crb_telephone1', 'Телефон 1')->set_width( 100 )
		       ->help_text('Введите номер телефона 1'),
		Field::make('text', 'crb_telephone2', 'Телефон 2')->set_width( 100 )
		       ->help_text('Введите номер телефона 2'),
		Field::make('text', 'crb_email_address', 'Email')->set_width( 100 )
		       ->help_text('Введите email'),
		
	))
 ->add_tab( 'Подключение метрик Goog;e', array(
		Field::make('textarea', 'crb_google_analitics', 'Код Google Analitics')->set_width( 100 )
		       ->help_text('Вставьте сюда код Google Analitics'),
 ));
 
/* ->add_tab( 'Подключение карты в Контакты', array(
		Field::make('text', 'crb_map_longitude', 'Коодинаты долготы')->set_width( 100 )
		       ->help_text('Вставьте сюда коодинаты долготы'),
		Field::make('text', 'crb_map_latitude', 'Коодинаты широты')->set_width( 100 )
		       ->help_text('Вставьте сюда коодинаты широты')			   
			   
));*/

