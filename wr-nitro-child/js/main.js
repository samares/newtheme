$('.header').ready(function() {
	var menu = $('.header').find('li');
	if(menu.length == 2) {
		$('.header').find('li').each(function() {
			if($(this).find('.menu_title').html() == 'Log In') $(this).addClass('login-btn');
			if($(this).find('.menu_title').html() == 'Sign Up') $(this).addClass('signup-btn');
		})
	}
})
$(document).ready(function() {
	if($('.camp-lbl-collected-val').length > 0) {
		// var perc = parseInt($('.camp-lbl-collected-val').html().slice(1));
  		// var fullSum = parseInt($('.camp-lbl-collected-goal').html().slice(1));
		// var fullSum = parseInt( $('.camp-lbl-collected-goal > .woocommerce-Price-amount').text().slice(1) );
		  var perc = parseInt( $('.camp-lbl-collected-val > .woocommerce-Price-amount').text().slice(1) );
		  var fullSum = parseInt( $('.camp-lbl-collected-goal').text().slice(1) );
		  var restSum = fullSum - perc;

  		var newBarWidth = perc*100/fullSum;
  		var newRestWidth = 100 - newBarWidth;
  		$('.camp-lbl-collected-rest').html('$'+restSum);
  		$('.camp-bar').css('width',newBarWidth+'%');
  		$('.camp-lbl-rest').css('width',newRestWidth+'%');
	}
  
})